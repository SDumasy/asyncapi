from asyncapi import main
import uuid
import os
import json
redis_host = '127.0.0.1'
from redis import Redis

if __name__ == '__main__':
    file = open("data.tsv", "r")
    pool = Redis(redis_host, port=6369, password=os.environ['KEY2'])
    for i, row in enumerate(file):
        x = row.split('\t')[0]
        file_name = 'imgs/' + x[x.rfind('/') + 2:]
        print(file_name)
        gen_uuid = str(uuid.uuid4())
        prop_dict = {
            'type': 'DownloadImg',
            'url': x,
            'dest': file_name,
            'job_id': gen_uuid
        }
        pool.lpush('properties', json.dumps(prop_dict))
        # if i >= 100:
        #     break

    status_dict = {
        'status': 'QUEUED',
    }
    pool.hset('status', gen_uuid, json.dumps(status_dict))
