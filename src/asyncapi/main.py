from fastapi import FastAPI, HTTPException
import time
import requests
import json
from redis import Redis
import os
import uuid
from pydantic import BaseModel
from concurrent.futures import ThreadPoolExecutor, as_completed



class Fibbo(BaseModel):
    type: str
    n: int

app = FastAPI()
redis_host = '127.0.0.1'

def Fibonacci(n):
    if n<0:
        print("Incorrect input")
    # First Fibonacci number is 0
    elif n==1:
        return 0
    # Second Fibonacci number is 1
    elif n==2:
        return 1
    else:
        return Fibonacci(n-1)+Fibonacci(n-2)

def download_img(url: str, dest: str, job_id: str):
    st_time = time.time()
    img_data = requests.get(url).content
    with open(dest, 'wb') as handler:
        handler.write(img_data)
        print(job_id + '---' + str(time.time() - st_time))

def start_worker():
    executor = ThreadPoolExecutor(max_workers=10)
    threaded = True
    start = time.time()
    pool = Redis(redis_host, port=6369, password=os.environ['KEY2'])
    while (resp := pool.lpop('properties')) is not None:
        res_dict = json.loads(resp.decode("utf-8"))
        status_dict = {
            'status': 'PICKED UP',
        }
        pool.hset('status', res_dict['job_id'], json.dumps(status_dict))
        if res_dict['type'] == 'fibbo':
            out = Fibonacci(res_dict['num'])
            print(out)
            status_dict['status'] = 'DONE'
            status_dict['response'] = out
            pool.hset('status', res_dict['job_id'], json.dumps(status_dict))
        elif res_dict['type'] == 'DownloadImg':
            if threaded:
                executor.submit(download_img, res_dict['url'], res_dict['dest'], res_dict['job_id'])
            else:
                download_img(res_dict['url'], res_dict['dest'])



if __name__ == '__main__':
    with ThreadPoolExecutor(max_workers=10) as execut:
        execut.submit(start_worker)


@app.post("/submitjob")
def get_download_usage_csv(fib: Fibbo):
    gen_uuid = str(uuid.uuid4())
    print(os.environ['KEY2'])
    prop_dict = {
        'type': fib.type,
        'num': fib.n,
        'job_id': gen_uuid
    }
    pool = Redis(redis_host, port=6369, password=os.environ['KEY2'])

    pool.lpush('properties', json.dumps(prop_dict))

    status_dict = {
        'status': 'QUEUED',
    }
    pool.hset('status', gen_uuid, json.dumps(status_dict))

    print(gen_uuid)

    return gen_uuid



@app.get("/status/")
def get_download_usage_csv(job_id: str):
    pool = Redis(redis_host, port=6369, password=os.environ['KEY2'])
    res = pool.hget('status', job_id)
    print(res)
    status = json.loads(res)

    return status
