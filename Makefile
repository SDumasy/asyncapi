release_major:
	bumpversion "major" setup.py

build:
	docker build -t $$(python setup.py --name):$$(python setup.py --version) .

run:
	uvicorn asyncapi.main:app --host 0.0.0.0 --port 81 --reload

kill:
	kill -9 $$(lsof -t -i:81 -sTCP:LISTEN)

