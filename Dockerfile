FROM python:3.7-slim

RUN apt-get update -y \
    && apt-get install -y gnupg curl \
    && apt-get update -y \
    && apt-get install -y \
        python3-dev python3-pip \
    && pip3 install --upgrade pip\
    && mkdir /app

ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
WORKDIR /app
ADD . /app/
RUN pip install .

EXPOSE 80
WORKDIR /app/src/usageapi/

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]
