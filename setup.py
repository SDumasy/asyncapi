from setuptools import setup, find_packages

setup(
    name="asyncapi",
    version="0.0.1",
    description="An async api experiment",
    author="Stephan Dumasy",
    package_dir={"": "src"},
    packages=find_packages('src'),
    zip_safe=False,
    install_requires=open('requirements.txt').read().splitlines(),
)